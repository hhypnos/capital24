@if (count($errors))
    <div class="form-group ">
        <div >
            <ul>
                <br>

                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>

                @endforeach

            </ul>
        </div>
    </div>
@endif
