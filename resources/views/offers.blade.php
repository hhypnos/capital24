<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143999754-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-143999754-1');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="{{env('APP_URL')}}" />

    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta property="og:url" content="{{env('APP_URL')}}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Capital24 Offers" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />

    {{--<link rel="apple-touch-icon" sizes="180x180" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="32x32" href="#">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="#">--}}
    {{--<link rel="shortcut icon" type="image/ico" href="{{URL::to('assets/images/fav.ico')}}">--}}
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{URL::to('icons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{URL::to('icons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#a3ce54">
    <meta name="theme-color" content="#ffffff">
    {{--<link rel="mask-icon" href="" color="#a3ce54">--}}
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#a3ce54">
    <meta name="description" content="Capital24 offers" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Capital24 offers" />
    <script type="application/ld+json">
        {
          "@context": "https:\/\/schema.org",
          "@type": "WebSite",
          "url": "{{url()->full()}}",
          "name": "Capital24 Offers",
          "contactPoint": {
            "@type": "ContactPoint",
            {{--"telephone": "+(995)-557-17-11-44",--}}
            "contactType": "Customer service"
          },
          "logo":"{{URL::to('assets/images/fav.ico')}}",
          "sameAs":["https:\/\/www.facebook.com\/Capital24-2353304841551884"]

        }
</script>
    <title>Capital24 Offers</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
{{--MOBILE SELECTOR--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.2/css/intlTelInput.css">
    {{--MOBILE SELECTOR--}}
    <link rel="stylesheet" href="{{URL::to('assets/css/style.css')}}">

 <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '407318393394196');
  fbq('track', 'landingPageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=407318393394196&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>

<div class="grid-container">
    <div class="left-side">
        <div class="logo">
            <img src="{{URL::to('assets/images/logo-white.png')}}">
            <p>Fund experts form a balanced investment portfolio for you
                Investment and withdrawal operations are fixded in the blockchain
                Most of the operations are performed automatically,
                starting from the moment of investment
                <br><br><br><br><br>
            <a href="{{URL::to('privacy.pdf')}}">Privacy Policy</a> </p>
        </div>

    </div>
    <div class="right-side">
        @if(is_null(session('emailSent')))
        <div class="form">
            <form method="post" action="{{route('send')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" class="form-control" name="name" id="formName" aria-describedby="nameHelp" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" id="formEmail" aria-describedby="emailHelp" placeholder="Your email">
                </div>

                <div class="form-group">
                    <input type="tel" class="form-control" name="phone" id="formPhone" aria-describedby="phoneHelp" placeholder="Your Phone Number">
                </div>

                <button type="submit" class="btn submit-btn">Send</button>
                   @include('layouts.errors')
            </form>
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        About us
                    </div>
                    <div class="col-sm">
                        <img src="{{URL::to('assets/images/fb-logo.png')}}">
                    </div>
                    <div class="col-sm">
                        Contact
                    </div>
                </div>
            </div>
        </div>
            @else
            @php(session()->forget('emailSent'))
            <div class="thanks">
                <h1>Thank You</h1>
                <h4>Your application was submitted
                    We will contact You Shortly</h4>
            </div>
            <script>
                fbq('track', 'landingCompleteRegistration');
            </script>
        @endif


    </div>
</div>




<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.2/js/intlTelInput.js"></script>
<script>

    @php($details = json_decode(file_get_contents("http://ipinfo.io/".request()->ip()."/json")))
    $(document).ready(() => {
        var input = document.querySelector("#formPhone");
        window.intlTelInput(input, {
            hiddenInput: "full_phone",
            initialCountry: "{{$details->country}}",
            // geoIpLookup: function(callback) {
            //     $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            //         var countryCode = (resp && resp.country) ? resp.country : "";
            //         callback(countryCode);
            //     });
            // },
            // utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.2/js/utils.js" // just for formatting/placeholders etc
        });
    });

</script>

</body>
</html>