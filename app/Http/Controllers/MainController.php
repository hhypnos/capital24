<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(){

//        return view('offers');

    }

    public function send(){
        $this->validate(request(),[
            'email'=>'nullable|email',
            'name'=>'max:30',
            'phone'=>'required'
        ]);
        $emailOptions = array(
            'email'=>request('email'),
            'name'=>request('name'),
            'phone' => request('text')
        );


        $details = json_decode(file_get_contents("http://ipinfo.io/".request()->ip()."/json"));
        $email = request('email');
        if(is_null($email)){
            $email = env('MAIL_USERNAME');
        }
        $subscribe = new Subscriber();
        $subscribe->full_name = request('name');
        $subscribe->email = request('email');
        $subscribe->phone = request('full_phone');
        $subscribe->country = $details->country;
        $subscribe->city = $details->city;
        $subscribe->save();

        \Mail::send([], $emailOptions, function($message) use ($email){
            $message->to('fotbz57@tutanota.com')->subject('Offer Capital24');
            $message->from($email);
            $message->setBody("<h1>email: ".request('email')."</h1><br><h1>name: ".request('name')."</h1><br><h1>phone: ".request('full_phone')."</h1>", 'text/html'); // for HTML rich messages
        });
        session(['emailSent'=>'ok']);
        return redirect('/');
    }
}
