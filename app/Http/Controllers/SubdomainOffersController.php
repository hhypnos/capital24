<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubdomainOffersController extends Controller
{
    public function index(){
        return view(mobile_view('offers','mobile_offers'));
    }
}
