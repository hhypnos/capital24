<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//MAIN DOMAIN
Route::group(array('domain' => 'capital24.co'), function () {

    Route::get('/', 'SubdomainOffersController@index');
});

Route::group(array('domain' => '127.0.0.1'), function () {

    Route::get('/', 'SubdomainOffersController@index');
});
//SUB-DOMAIN

Route::group(array('domain' => 'offers.capital24.co'), function () {
    Route::get('/', 'SubdomainOffersController@index');
});




Route::post('send', 'MainController@send')->name('send');
